/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Nov 5, 2019, 9:19:01 AM                     ---
 * ----------------------------------------------------------------
 */
package com.sefam.sefamwishlistfacades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedSefamwishlistfacadesConstants
{
	public static final String EXTENSIONNAME = "sefamwishlistfacades";
	
	protected GeneratedSefamwishlistfacadesConstants()
	{
		// private constructor
	}
	
	
}
