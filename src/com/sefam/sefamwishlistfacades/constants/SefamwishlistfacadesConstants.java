/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sefam.sefamwishlistfacades.constants;

/**
 * Global class for all Sefamwishlistfacades constants. You can add global constants for your extension into this class.
 */
public final class SefamwishlistfacadesConstants extends GeneratedSefamwishlistfacadesConstants
{
	public static final String EXTENSIONNAME = "sefamwishlistfacades";

	private SefamwishlistfacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "sefamwishlistfacadesPlatformLogo";
}
