/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sefam.sefamwishlistfacades.setup;

import static com.sefam.sefamwishlistfacades.constants.SefamwishlistfacadesConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.sefam.sefamwishlistfacades.constants.SefamwishlistfacadesConstants;
import com.sefam.sefamwishlistfacades.service.SefamwishlistfacadesService;


@SystemSetup(extension = SefamwishlistfacadesConstants.EXTENSIONNAME)
public class SefamwishlistfacadesSystemSetup
{
	private final SefamwishlistfacadesService sefamwishlistfacadesService;

	public SefamwishlistfacadesSystemSetup(final SefamwishlistfacadesService sefamwishlistfacadesService)
	{
		this.sefamwishlistfacadesService = sefamwishlistfacadesService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		sefamwishlistfacadesService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return SefamwishlistfacadesSystemSetup.class.getResourceAsStream("/sefamwishlistfacades/sap-hybris-platform.png");
	}
}
