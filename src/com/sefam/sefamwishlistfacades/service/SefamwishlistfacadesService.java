/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.sefam.sefamwishlistfacades.service;

public interface SefamwishlistfacadesService
{
	String getHybrisLogoUrl(String logoCode);

	void createLogo(String logoCode);
}
